# Proyecto Spring Boot SOAP

Este proyecto es una aplicación Spring Boot que consume una API REST y proporciona un endpoint SOAP para obtener información sobre Pokémon.

## Tecnologías y Herramientas Utilizadas

- **Java 17**
- **Spring Boot 3.2.5**
- **Spring Web**
- **Spring Web Services**
- **Spring Data JPA**
- **H2 Database**
- **Maven**
- **SOAP UI** (para pruebas SOAP)
- **Postman** (para pruebas REST)

## Prerrequisitos

Antes de ejecutar el proyecto, asegúrate de tener instalados los siguientes programas:

- **Java 17**
- **Maven 3.6.x o superior**

## Configuración del Proyecto

1. **Clonar el repositorio:**

   ```sh
   git clone https://gitlab.com/kristianlom/bankaya-pokemon.git
   cd bankaya-pokemon
   ```

2. **Configurar las propiedades de la aplicación:**

   El proyecto está configurado para usar una base de datos H2 en memoria por defecto. Puedes ajustar las propiedades en el archivo `src/main/resources/application.properties` según tus necesidades.

   ```properties
   spring.datasource.url=jdbc:h2:mem:testdb
   spring.datasource.driverClassName=org.h2.Driver
   spring.datasource.username=sa
   spring.datasource.password=password
   spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
   spring.h2.console.enabled=true
   spring.h2.console.path=/h2-console
   ```

## Ejecutar el Proyecto

1. **Compilar y empaquetar el proyecto:**

   Desde la raíz del proyecto, ejecuta:

   ```sh
   mvn clean install
   ```

2. **Ejecutar la aplicación:**

   ```sh
   mvn spring-boot:run
   ```

3. **Acceder a la consola H2:**

   Puedes acceder a la consola H2 para ver la base de datos en memoria en la siguiente URL:

   ```
   http://localhost:8080/h2-console
   ```

   Utiliza los siguientes datos de conexión:

   - **JDBC URL:** `jdbc:h2:mem:testdb`
   - **User Name:** `sa`
   - **Password:** `password`

## Endpoints

### Endpoint REST

Para obtener información sobre un Pokémon utilizando el endpoint REST, puedes usar Postman o cURL:

```sh
curl -X GET "http://localhost:8080/pokemon/{name}" -H "accept: application/json"
```

Reemplaza `{name}` con el nombre del Pokémon que deseas buscar.

### Endpoint SOAP

Para obtener información sobre un Pokémon utilizando el endpoint SOAP, puedes usar SOAP UI o cualquier cliente SOAP. Aquí tienes un ejemplo de solicitud SOAP:

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:pok="http://lopezkristian.com/pokemon">
   <soapenv:Header/>
   <soapenv:Body>
      <pok:PokemonRequest>
         <pok:name>pikachu</pok:name>
      </pok:PokemonRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

El WSDL del servicio está disponible en la siguiente URL:

```
http://localhost:8080/ws/pokemon.wsdl
```

## Pruebas

### Pruebas Unitarias

Para ejecutar las pruebas unitarias, utiliza el siguiente comando:

```sh
mvn test
```

### Pruebas SOAP

Utiliza SOAP UI para enviar solicitudes SOAP al endpoint y verificar las respuestas.

### Pruebas REST

Utiliza Postman o cURL para enviar solicitudes REST y verificar las respuestas.

## Contacto

Si tienes alguna pregunta o sugerencia, no dudes en contactarme a [kristian.lom@gmail.com](mailto:kristian.lom@gmail.com).

Kristian López.