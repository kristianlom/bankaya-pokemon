package com.lopezkristian.pokemon.model;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"id","name","baseExperience","locationAreaEncounters", "weight"})
@XmlRootElement(name = "PokemonResponse", namespace = "http://lopezkristian.com/pokemon")
public class PokemonResponse {

    @XmlElement(required = true)
    protected int id;

    @XmlElement(required = true)
    protected String name;

    @XmlElement(required = true)
    protected int baseExperience;

    @XmlElement(required = true)
    protected String locationAreaEncounters;

    @XmlElement(required = true)
    protected int weight;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBaseExperience() {
        return baseExperience;
    }

    public void setBaseExperience(int baseExperience) {
        this.baseExperience = baseExperience;
    }

    public String getLocationAreaEncounters() {
        return locationAreaEncounters;
    }

    public void setLocationAreaEncounters(String locationAreaEncounters) {
        this.locationAreaEncounters = locationAreaEncounters;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
