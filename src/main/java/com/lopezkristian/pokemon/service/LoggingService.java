package com.lopezkristian.pokemon.service;

import com.lopezkristian.pokemon.model.RequestLog;
import com.lopezkristian.pokemon.repository.RequestLogRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class LoggingService {

    private final RequestLogRepository requestLogRepository;

    public LoggingService(RequestLogRepository requestLogRepository) {
        this.requestLogRepository = requestLogRepository;
    }

    public void logRequest(HttpServletRequest request, String method) {
        RequestLog log = new RequestLog();
        log.setIp(request.getRemoteAddr());
        log.setRequestDate(LocalDateTime.now());
        log.setMethod(method);
        requestLogRepository.save(log);
    }
}
