package com.lopezkristian.pokemon.service;

import com.lopezkristian.pokemon.model.Pokemon;
import com.lopezkristian.pokemon.model.PokemonResponse;
import org.springframework.stereotype.Service;

@Service
public class PokemonSoapService {

    private final PokeApiService pokeApiService;

    public PokemonSoapService(PokeApiService pokeApiService) {
        this.pokeApiService = pokeApiService;
    }

    public PokemonResponse getPokemon(String name) {
        Pokemon pokemon = pokeApiService.getPokemon(name);

        PokemonResponse response = new PokemonResponse();

        response.setId(pokemon.getId());
        response.setName(pokemon.getName());
        response.setBaseExperience(pokemon.getBase_experience());
        response.setLocationAreaEncounters(pokemon.getLocation_area_encounters());
        response.setWeight(pokemon.getWeight());

        return response;
    }
}
