package com.lopezkristian.pokemon.repository;

import com.lopezkristian.pokemon.model.RequestLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestLogRepository extends JpaRepository<RequestLog, Long> {
}
