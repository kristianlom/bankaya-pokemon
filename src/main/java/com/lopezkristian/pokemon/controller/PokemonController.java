package com.lopezkristian.pokemon.controller;

import com.lopezkristian.pokemon.model.Pokemon;
import com.lopezkristian.pokemon.service.PokeApiService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PokemonController {

    private final PokeApiService pokeApiService;

    public PokemonController(PokeApiService pokeApiService) {
        this.pokeApiService = pokeApiService;
    }

    @GetMapping("/pokemon/{name}")
    public Pokemon getPokemon(@PathVariable String name) {
        return pokeApiService.getPokemon(name);
    }
}
