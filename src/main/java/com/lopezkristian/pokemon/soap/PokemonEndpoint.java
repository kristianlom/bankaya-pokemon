package com.lopezkristian.pokemon.soap;

import com.lopezkristian.pokemon.service.LoggingService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.xml.ws.Action;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.lopezkristian.pokemon.service.PokemonSoapService;
import com.lopezkristian.pokemon.model.PokemonRequest;
import com.lopezkristian.pokemon.model.PokemonResponse;
import org.springframework.ws.soap.SoapMessage;

@Endpoint
public class PokemonEndpoint {

    private static final String NAMESPACE_URI = "http://lopezkristian.com/pokemon";

    private final PokemonSoapService pokemonSoapService;
    private final LoggingService loggingService;
    private final HttpServletRequest request;

    public PokemonEndpoint(PokemonSoapService pokemonSoapService, LoggingService loggingService, HttpServletRequest request) {
        this.pokemonSoapService = pokemonSoapService;
        this.loggingService = loggingService;
        this.request = request;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "PokemonRequest")
    @ResponsePayload
    public PokemonResponse getPokemon(@RequestPayload PokemonRequest request) {
        loggingService.logRequest(this.request, "getPokemon");

        return pokemonSoapService.getPokemon(request.getName());
    }

    @Action(output = "PokemonRequest")
    public void handlePokemonRequest(SoapMessage request) {
       request.setSoapAction("http://lopezkristian.com/pokemon/getPokemon");
    }
}
