package com.lopezkristian.pokemon.service;

import com.lopezkristian.pokemon.model.Pokemon;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PokeApiServiceTests {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private PokeApiService pokeApiService;

    @Test
    void testGetPokemon() {
        String pokemonName = "pikachu";
        String url = "https://pokeapi.co/api/v2/pokemon/" + pokemonName;

        Pokemon mockPokemon = new Pokemon();
        mockPokemon.setName(pokemonName);
        mockPokemon.setBase_experience(112);

        when(restTemplate.getForObject(url, Pokemon.class)).thenReturn(mockPokemon);

        Pokemon pokemon = pokeApiService.getPokemon(pokemonName);

        assertNotNull(pokemon);
        assertEquals(pokemonName, pokemon.getName());
        assertEquals(112, pokemon.getBase_experience());
    }
}
